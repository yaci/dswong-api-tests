# dswong-api-tests

Example project, similar to what we've coded together during DSWONG on 14 August 2018. Created in IntelliJ, but you can run it also from Eclipse or Visual Studio Code. All of these tools are free.

To run from command line execute: `./gradlew clean test --no-build-cache`

The test generates html report under dswong-api-tests/build/reports/tests/test/index.html
To see the full path to the report you can execute the above command with additional `--info` parameter.
