package pl.yaci;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.junit.Test;

public class ExampleTest {


    @Test
    public void exampleApiTestMethod() {
        // we query an example api in this case httpbin.org which is a service
        // for learning APIs and HTTP.
        // httpbin.org/anything returns exactly what was passed in a request to
        // the server. The example json schema requires a field "fruit" to be
        // present in response, so we pass it in a header.

        given()
            .header("fruit", "watermelon")
        .when()
            .get("http://httpbin.org/anything")
        .then()
            .statusCode(200)
        .and()
            .body(matchesJsonSchemaInClasspath("schemas/example-schema.json"));
    }

}
